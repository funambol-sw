@echo off

rem -- Define the path to a make utility on your computer
set MAKE=nmake.exe

rem -- RIM's signature tool to sign the .cod file
set SIGN=c:\progra~1\resear~1\blackb~1.1\bin\SignatureTool.jar

rem -- Define the path to the ant utility on your computer
set ANT=ant

rem -- In order to compile, the JDK must be in your path. If this is
rem -- not set in your environment, uncomment and adjust the following
set JAVA_HOME=C:\Progra~1\Java\jdk15~1.0_1
set PATH=%PATH%;%JAVA_HOME%\bin

set PRJNAME=funambol_blackberry_plugin

rem -- "/s" delete subfolders, "/q" delete in quiet mode (without asking) 
del /s/q buildrelease

mkdir buildrelease

%MAKE% -f %PRJNAME%.mak

java -jar %SIGN% -a %PRJNAME%.cod

move %PRJNAME%.alx buildrelease\%PRJNAME%.alx
move %PRJNAME%.cod buildrelease\%PRJNAME%.cod
move %PRJNAME%.csl buildrelease\%PRJNAME%.csl
move %PRJNAME%.cso buildrelease\%PRJNAME%.cso
move %PRJNAME%*.debug buildrelease\
move %PRJNAME%.jar buildrelease\%PRJNAME%.jar
move %PRJNAME%.jad buildrelease\%PRJNAME%.jad
copy LICENSE.txt buildrelease

%ANT% -f build\build.xml
