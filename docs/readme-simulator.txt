Funambol BlackBerry Plug-in Community Edition v. 3.0.8

HOW TO run the Funambol Plug-in on the BlackBerry Simulator
================================================================================

1. Download and install the BlackBerry Java Development Environment (JDE) 4.2.0
   from http://na.blackberry.com/eng/developers/downloads/jde.jsp

2. Copy

       - funambol_blackberry_plugin.cod
       - funambol_blackberry_plugin.csl
       - funambol_blackberry_plugin.cso
       - funambol_blackberry_plugin.debug
       - funambol_blackberry_plugin-1.debug
       - funambol_blackberry_plugin.jar
   
in [Research_In_Motion_HOME]\BlackBerry JDE 4.2.0\simulator

3. Start the Simulator (Start > Programs > Research In Motion >
   BlackBerry JDE 4.2.0 > Device Simulator)

4. The application will be installed in the Simulator as "Funambol 3.0.8".

5. Launch the application and configure it.

6. Start the synchronization selecting the "Synchronize" manu item.

7. Test the application

Note: If you use the BlackBerry MDS Simulator, be aware that it listens to
connections on the TCP port 8080. This may conflict with other applications,
such as Tomcat or the Funambol DS Server bundled edition.


NOTE
====

(This information is no longer up-to-date, and is maintained here only for
historical purposes)

- BlackBerry with OS version less than 4.0.2 does not support multiple address
recognition. Applies to: BlackBerry OS version earlier than 4.0.2 and Sync4j
BlackBerry Synchronizer version earlier than 1.1.5, Contact Database 

Description: Due to the limitations of the BlackBerry OS in versions earlier
than 4.0.2, the BlackBerry does not support the multiple address field
recognition that is necessary for synchronization. With version 4.0.2, the PIM
functionality was enhanced where additional access to fields was granted. In
order to take advantage of this, version 4.0.2 must be installed onto the
BlackBerry handheld. In addition to 4.0.2 (or higher) for the BlackBerry, you
must have version 1.1.5 or higher of the Sync4j BlackBerry Client. 

What this means is that if you have version 4.0.2 or higher of the BlackBerry OS
and version 1.1.5 or higher of the Sync4j BlackBerry Synchronizer, then your
address data will be synchronized. If you have version 4.0.0.x or 4.0.1.x of the
BlackBerry OS and version 1.1.5 or higher of the Sync4j BlackBerry Synchronizer,
then the address data will be ignored in the synchronization. In the latter
scenario, no address data would be updated to or from the Portal to your
BlackBerry device to avoid data compromisation. 

If you do not have either of the previously described combinations of BlackBerry
OS and Sync4j BlackBerry Synchronizer software, then we advise that you do not
synchronize your contacts unless you have no address data or only work
addresses. Otherwise, due to the BlackBerry OS's inability to recognize multiple
addresses, your address data may be compromised, i.e. you may lose address data 
when syncing back from the Portal to your device.

To install or upgrade to the latest version of the Sync4j BlackBerry
Synchronizer, please visit the downloads page.

- BlackBerry OS (API) does not support web page recognition. Applies to:
BlackBerry OS version 4.0.2 and all earlier versions, Contact Database

Description: Due to the limitations of the BlackBerry OS (API) in version 4.0.2
and all earlier versions, the BlackBerry does not support the web page field
recognition that is necessary for synchronization. Therefore, until BlackBerry
adds this field to their API, it cannot be synchronized by the Sync4j BlackBerry
Synchronizer. Thus, when synchronizing your data, the web page data will not be
passed back and forth between your device and the Portal. This field data will
remained unchanged.
