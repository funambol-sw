/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.util;

import com.funambol.syncclient.blackberry.SyncClient;

import net.rim.device.api.system.EventLogger;


/**
 * Logger class.
 */
public class Log
{
    //----------------------------------------------------------------- Constants
    public static final int ERROR = 0;
    public static final int INFO = 1;
    public static final int DEBUG = 2;
    public static final int TRACE = 3;

    /*
     * Default debug level
     */
    private static int level = INFO;

    /*
     * Enable output on stdout for debugger
     */
    private static boolean debug = true;

    //--------------------------------------------------------------- Accessor methods
    public static void setLevel(int newlevel) { level = newlevel; }

    public static int getLevel() { return level; }

    public static void enableDebugger() { debug = true; }

    public static void disableDebugger() { debug = false; }

    public static boolean isDebuggerEnabled() { return debug; }

    //----------------------------------------------------------------- Public methods
    public static void error(String msg) {
        writeMessage("ERROR", msg);
    }

    public static void info(String msg) {
        if (level >= INFO) {
            writeMessage("INFO", msg);
        }
    }

    public static void debug(String msg) {
        if (level >= DEBUG) {
            writeMessage("DEBUG", msg);
        }
    }

    public static void trace(String msg) {
        if (level >= TRACE) {
            writeMessage("TRACE", msg);
        }
    }

    //----------------------------------------------------------------- Private methods

    /**
     * Logs a string to standard output and to the device's event logger
     * (press ALT + lglg on the device to see the event log)
     *
     * @param msg the string to be logged
     **/
    private static void writeMessage(String level, String msg)
    {
        msg = "[" + level + "] " + msg;

        if (debug)
            System.out.println(msg);
           
        //com.funambol.syncclient.util.StaticDataHelper            
        long guid = 0x948b87e2ee064ebeL;
        
        try
        {
            EventLogger.register(guid, 
                                 "Funambol " + SyncClient.VER, 
                                 EventLogger.VIEWER_STRING);
        }
        catch (Throwable t)
        {
            System.out.println(">>> Exception by registering the event logger:\n" +
                               ">>> Error message: " + t.getMessage() + "\n" +
                               ">>> Short description: " + t.toString() + "\n");
            t.printStackTrace();
        }
        
        try
        {
            if (msg != null)
            {
                EventLogger.logEvent(guid, msg.getBytes());
            }
            else
            {
                System.out.println(">>> [DEBUG] String msg is " + msg);
            }
        }
        catch (Throwable tt)
        {
            System.out.println(">>> Exception by registering the event logger:\n" +
                               ">>> Error message: " + tt.getMessage() + "\n" +
                               ">>> Short description: " + tt.toString() + "\n");
            tt.printStackTrace();
        }
    }
    
}

