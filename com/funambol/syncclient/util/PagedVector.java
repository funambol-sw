/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.util;

import java.util.Vector;

/**
 * Represents a Vector where the items can be accessed in a paged 
 * way, so that the items can be accessed page per page in once
 * 
 * 
 */
public class PagedVector extends Vector {
    
    private int page;
    private int nextPageIndex;

    /**
     * Creates a new PagedVector
     *
     */
    public PagedVector(int page) {
      
        this.page = page;
        this.nextPageIndex = 0;
    }
    
    /**
     *  Creates a new PagedVector
     *
     *  @param v Input vector
     *  @param page Number of elements per page
     */
    public PagedVector(Vector v, int page) {
      
        this(page);
        
        for (int i = 0, s = v.size(); i < s; ++i) {
            
            addElement(v.elementAt(i));
        }
    }

    /**
     * Fills the given Vector with the elements of the next page
     * 
     * @return boolean <code>true</code> if there are more elements, <code>false</code> otherwise
     */
    public boolean getNextPage(final Vector elements) {
        
        boolean ret = true;

        int s = size();

        elements.removeAllElements(); // just in case...
        for (int i=0 ; i < page; ++i) {
            
            if (nextPageIndex >= s)
            {
                ret = false;
                break;
            }
            elements.addElement(elementAt(nextPageIndex++));
        }
        
        return ret;
    }

    /**
     * Resets the current position
     *
     */
    public void reset() {
        nextPageIndex = 0;
    }

    /**
     * Are we at the end of the vector?
     *
     * @return true if the last item was returned in the last call to 
     *         getNextPage, false otherwise
     */
    //public boolean hasMore;
} 
