/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.util;

import java.util.Hashtable;
import java.io.InputStream;
import java.io.IOException;

import com.funambol.syncclient.blackberry.SyncClient;

import net.rim.device.api.system.*;


/**
 * Supports general using, language dictionary access,
 * logging. Statics data and static methods
 */
public class StaticDataHelper
{
    //----------------------------------------------------------------- Constants

    final private static String KEY_HOMEPAGE               = "homepage-default"            ;
    final private static String KEY_LOGIN                  = "login-default"               ;
    final private static String KEY_CONTACT_SOURCE_URI     = "contact-source-uri-default"  ;
    final private static String KEY_CALENDAR_SOURCE_URI    = "calendar-source-uri-default" ;
    final private static String KEY_MAIL_SOURCE_URI        = "mail-source-uri-default"     ;
    final private static String KEY_GATEWAY_APN            = "gateway-apn-default"         ;
    final private static String KEY_GATEWAY_IP             = "gateway-ip-default"          ;
    final private static String KEY_GATEWAY_PORT           = "gateway-port-default"        ;
    final private static String KEY_ALERTING_PORT          = "alerting-port-default"       ;
    final private static String KEY_ENABLE_POLLING_DEFAULT = "enable-polling-default"      ;//CHANGE 17-03-2006 line added
    final private static String KEY_ENABLE_AUTOSYNC_DEFAULT = "enable-autosync-default"      ;
    final private static String KEY_HOST_IP                = "host-ip-default"             ;
    final private static String KEY_SYNC_CONTACT           = "sync-contact-default"        ;
    final private static String KEY_SYNC_CALENDAR          = "sync-calendar-default"       ;
    final private static String KEY_SYNC_MAIL              = "sync-mail-default"           ;
    final private static String KEY_ENCODE                 = "encode-default"              ;
    final private static String KEY_EMAIL_ID               = "email-id-default"          ;
    final private static String HOST_PORT_DEFAULT          = "8080"                        ;
    final private static String GATEWAY_PORT_DEFAULT       = "9201"                        ;
    final private static String KEY_ENCRYPTION_DEFAULT     = "encryption-type-default"     ;
    final private static String XML_LANGUAGE               = "/xml/language.xml"           ;

   //----------------------------------------------------------------- Private data

    private static String homePageDefault              = null  ;
    private static String loginDefault                 = null  ;
    private static String userDefault                  = null  ;
    private static String passwordDefault              = null  ;
    private static String contactSourceUriDefault      = null  ;
    private static String calendarSourceUriDefault     = null  ;
    private static String gatewayApnDefault            = null  ;
    private static String gatewayIpDefault             = null  ;
    private static String gatewayPortDefault           = null  ;
    private static boolean syncContactDefault          = false ;
    private static boolean syncCalendarDefault         = false ;
    private static boolean enablePollingDefault        = false ;
    private static boolean enableAutoSyncDefault        = false ;
    private static boolean smsServerAlertedSyncDefault = false ;
    private static boolean encodeDefault               = false ;
    private static String encryptDefault               = null  ;
    private static String operatingSystem              = null  ;
    
    //additional
    private static String mailSourceUriDefault = null  ;
    private static boolean syncMailDefault     = false ;
    private static String alertingPortDefault  = null  ;
    private static String hostIpDefault        = null  ;
    private static String emailIdDefault       = null  ;
    
    /*
     * debug enable
     */
    private static boolean debug = true;

    /* 
     * language words
     */
    private static String language = null;

    /*
     * language words
     */
    private static Hashtable languageHash = new Hashtable();

    //----------------------------------------------------------------- Public methods

    public String getMailSourceUriDefault()
    {
        return mailSourceUriDefault;
    }
    
    public boolean getSyncMailDefault()
    {
        return syncMailDefault;
    }
    
    public String getHomePageDefault()
    {
        return homePageDefault;
    }

    public String getUserDefault()
    {
        return userDefault;
    }
    
    public String getPasswordDefault()
    {
        return passwordDefault;
    }
    
    public String getContactSourceUriDefault()
    {
        return contactSourceUriDefault;
    }

    public String getCalendarSourceUriDefault()
    {
        return calendarSourceUriDefault;
    }

    public String getGatewayApnDefault()
    {
        return gatewayApnDefault;
    }
    
    public String getEncryptDefault()
    {
        return encryptDefault;
    }
    
    public String getGatewayIpDefault()
    {
        return gatewayIpDefault;
    }

    public String getGatewayPortDefault()
    {
        return gatewayPortDefault;
    }
    
    public String getAlertingPortDefault()
    {
        return alertingPortDefault;
    } 
    
    public String getHostIpDefault()
    {
        return hostIpDefault;
    }
     
    public String getEmailIdDefault()
    {
        return emailIdDefault;
    }
    
    public boolean getSyncContactDefault()
    {
        return syncContactDefault;
    }
   
    public boolean getSyncCalendarDefault()
    {
        return syncCalendarDefault;
    }

    public boolean getEnablePollingDefault() {
        return enablePollingDefault;
    }
    
    public boolean getEnableAutoSyncDefault() {
        return enableAutoSyncDefault;
    }
    
    public boolean getSmsServerAlertedSyncDefault() {
       return smsServerAlertedSyncDefault;
    }

    public String getOS()
    {
        return operatingSystem;
    }
    
    public void setOS()
    {
        ApplicationDescriptor[] appDes = ApplicationManager.getApplicationManager().getVisibleApplications();

        String version = "";

        for (int i = appDes.length - 1; i >= 0; --i)
        {
            if ((appDes[i].getModuleName()).equals("net_rim_bb_ribbon_app"))
            {
                version = appDes[i].getVersion();
            }
        }
        operatingSystem = version;
    }

    public boolean isEncode()
    {
        return encodeDefault;
    }

    /**
     * Logs a string to standard output and to the device's event logger
     * (press ALT + lglg on the device to see the event log)
     *
     * @param msg the string to be logged
     **/
    public static void log(String msg)
    {
        if (debug)
            System.out.println(msg);
           
        //com.funambol.syncclient.util.StaticDataHelper            
        long guid = 0x948b87e2ee064ebeL;
        
        try
        {
            EventLogger.register(guid, 
                                 "Funambol " + SyncClient.VER, 
                                 EventLogger.VIEWER_STRING);
        }
        catch (Throwable t)
        {
            System.out.println(">>> Exception by registering the event logger:\n" +
                               ">>> Error message: " + t.getMessage() + "\n" +
                               ">>> Short description: " + t.toString() + "\n");
            t.printStackTrace();
        }
        
        try
        {
            if (msg != null)
            {
                EventLogger.logEvent(guid, msg.getBytes());
            }
            else
            {
                System.out.println(">>> [DEBUG] String msg is " + msg);
            }
        }
        catch (Throwable tt)
        {
            System.out.println(">>> Exception by registering the event logger:\n" +
                               ">>> Error message: " + tt.getMessage() + "\n" +
                               ">>> Short description: " + tt.toString() + "\n");
            tt.printStackTrace();
        }
    }

    /**
     * Make a String by value of <i>tag</i>.
     *
     * @param key key to find
     * @return key value
     **/
    public static String getLanguage(String key) {

        String startTag      = null ;
        String endTag        = null ;

        String value         = null ;

        Object valueFromHash = null ;

        valueFromHash = languageHash.get(key);

        if (valueFromHash != null) {
            return (String)valueFromHash;
        }

        startTag = "<"  + key + ">" ;
        endTag   = "</" + key + ">" ;

        String msg = "Parsing XML, TAG " + key ;
        StaticDataHelper.log (msg);

        //CHANGE 17-03-2006 ------------------------------------------------------
        if (language.indexOf(startTag) == - 1 || language.indexOf(endTag) == -1) {
            StaticDataHelper.log("Language tag " + startTag + " not found");
            return "";
        }
        // -----------------------------------------------------------------------
        
        value = language.substring
                (language.indexOf(startTag) + startTag.length(), language.indexOf(endTag)).trim();

        languageHash.put(key, value);

        return value;

    }

    /**
     * load xml language file
     */
    public static void loadLanguage (Class c)
    throws Exception{
        try {
            language = read(c.getResourceAsStream(XML_LANGUAGE));
        } catch (IOException e ) {

            String msg = "Error: load "         +
                         XML_LANGUAGE           +
                         " file: "              +
                         e.getMessage()         ;

            StaticDataHelper.log (msg);
            throw new Exception("Error reading language definition file.");
        }
    }

    /**
     * Reads the content of the given input stream.
     *
     * @param is the input stream
     **/
    public static String read(InputStream is) throws IOException {
        StringBuffer sb = new StringBuffer();

        try {

            byte[] buf = new byte[1024];

            int nbyte = -1;
            while ((nbyte = is.read(buf)) >= 0) {
                sb.append(new String(buf, 0, nbyte));
            }
        } finally {
            is.close();
        }

        return sb.toString();
    }

    /**
     * Load default properties form jad file
     */
    public void loadDefaultValue()
    throws Exception {

        String enableContact  = null ;
        String enableCalendar = null ;
        String enableMail     = null ;
        String enablePolling  = null ;//CHANGE 17-03-2006
        String enableAutoSync = null ;
        String encode         = null ;
        

        homePageDefault          = getLanguage(KEY_HOMEPAGE            );
        loginDefault             = getLanguage(KEY_LOGIN               );
        contactSourceUriDefault  = getLanguage(KEY_CONTACT_SOURCE_URI  );
        calendarSourceUriDefault = getLanguage(KEY_CALENDAR_SOURCE_URI );
        gatewayApnDefault        = getLanguage(KEY_GATEWAY_APN         );
        gatewayIpDefault         = getLanguage(KEY_GATEWAY_IP          );
        gatewayPortDefault       = getLanguage(KEY_GATEWAY_PORT        );
        alertingPortDefault      = getLanguage(KEY_ALERTING_PORT       );
        hostIpDefault            = getLanguage(KEY_HOST_IP             );
        enableContact            = getLanguage(KEY_SYNC_CONTACT        );
        enableCalendar           = getLanguage(KEY_SYNC_CALENDAR       );
        enablePolling            = getLanguage(KEY_ENABLE_POLLING_DEFAULT);//CHANGE 17-03-2006 line added
        enableAutoSync            = getLanguage(KEY_ENABLE_AUTOSYNC_DEFAULT);
        encode                   = getLanguage(KEY_ENCODE              );
        emailIdDefault           = getLanguage(KEY_EMAIL_ID            );
        mailSourceUriDefault     = getLanguage(KEY_MAIL_SOURCE_URI     );
        enableMail               = getLanguage(KEY_SYNC_MAIL           );
        encryptDefault           = getLanguage(KEY_ENCRYPTION_DEFAULT  );
        if (homePageDefault == null && !(homePageDefault.length() > 0)) {
            homePageDefault = "";
        }

        if (loginDefault == null) {
            userDefault     = "" ;
            passwordDefault = "" ;
        } else if ((loginDefault.length() > 0) && loginDefault.indexOf(":") == -1) {
            String msg = "Error: load jad properties wrong login";
            StaticDataHelper.log (msg);
            throw new Exception(msg);
        }

        userDefault     = loginDefault.substring(0, loginDefault.indexOf(":")  );
        passwordDefault = loginDefault.substring(loginDefault.indexOf(":") + 1 );

        if (contactSourceUriDefault == null) {
            contactSourceUriDefault = "";
        }

        if (calendarSourceUriDefault == null) {
            calendarSourceUriDefault = "";
        }

        if (gatewayApnDefault == null) {
            gatewayApnDefault = "";
        }
      
        if (gatewayIpDefault == null) {
            gatewayIpDefault = "";
        }

        if (gatewayPortDefault == null || !(gatewayPortDefault.length() > 0)) {
            gatewayPortDefault = GATEWAY_PORT_DEFAULT;
        } else {
            try {
                Integer.parseInt(gatewayPortDefault);
            } catch (NumberFormatException e){
                String msg = "Error: load jad properties wrong gateway-port";
                StaticDataHelper.log (msg);
                throw new Exception(msg);
            }
        }
        if(encryptDefault == null) {
          encryptDefault ="none";  
        }
        if ("true".equals(enableContact))  {
            syncContactDefault = true;
        }

        if ("true".equals(enableCalendar)) {
            syncCalendarDefault = true;
        }
        if("true".equals(enableMail))      {
            syncMailDefault = true;
        }
        
        //CHANGE 17-03-2006 -------------------
        if("true".equals(enablePolling))      {
            enablePollingDefault = true;
        }// -----------------------------------
        
        if("true".equals(enableAutoSync))      {
            enableAutoSyncDefault = true;
        }
        
        if ("true".equals(encode))         {
            encodeDefault = true;
        }

    }

}
