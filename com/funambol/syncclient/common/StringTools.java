/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.common;

import com.funambol.syncclient.util.StaticDataHelper;

/**
 * Utility class that groups string manipulation functions.
 *
 */
public class StringTools {

    /**
     * <p>Escapes the characters in a <code>String</code> using XML entities.</p>
     *
     *
     * <p>Supports only the four basic XML entities (gt, lt, quot, amp).
     * Does not support DTDs or external entities.</p>
     *
     * @param str  the <code>String</code> to escape, may be null
     * @return a new escaped <code>String</code>, <code>null</code> if null string input
     * @see #unescapeXml(java.lang.String)
     **/
    public static String escapeXml(String str) {
        if (str == null) {
            return null;
        }
        return Entities.XML.escape(str);
    }

    /**
     * <p>Unescapes a string containing XML entity escapes to a string
     * containing the actual Unicode characters corresponding to the
     * escapes.</p>
     *
     * <p>Supports only the four basic XML entities (gt, lt, quot, amp).
     * Does not support DTDs or external entities.</p>
     *
     * @param str  the <code>String</code> to unescape, may be null
     * @return a new unescaped <code>String</code>, <code>null</code> if null string input
     * @see #escapeXml(String)
     **/
    public static String unescapeXml(String str) {
        if (str == null) {
            return null;
        }
        return Entities.XML.unescape(str);
    }

    public static String QPdecode(String qp)
    {
        int i;
        StringBuffer ret = new StringBuffer();

        for (i = 0; i<qp.length(); i++ ) {
            // Handle encoded chars
            if ( qp.charAt(i) == '=' ) {
                if (qp.length() - i > 2 ) {
                    // The sequence is complete, check it
                    //i++;   // skip the '='
                    String code = qp.substring(i+1, i+3).trim();

                    if (code.equals("\r\n")) {
                        // soft line break, ignore it
                        i += 2;
                        continue;
                    }
                    else {
                        try {
                            char c = (char)(Byte.parseByte(code.toLowerCase(), 16));
                            ret.append(c);
                            i += 2;
                            continue;
                        }
                        catch (NumberFormatException nfe){
                            StaticDataHelper.log("QPDecode: Invalid sequence =" + code);
                        }
                    }
                }
                // In all wrong cases leave the original bytes
                // (see RFC 2045). They can be incomplete sequence,
                // or a '=' followed by non hex digit.
            }
            // TODO:
            // RFC 2045 says to exclude control characters mistakenly
            // present (unencoded) in the encoded stream.
            
            // Copy other characters
            ret.append(qp.charAt(i));
        }
        
        return ret.toString();
    }
    
    public static String dump(String[] array) {
        StringBuffer buffer = new StringBuffer("Content of array " + 
                                               array.toString() + 
                                               ":\n");        
        for (int i = 0; i < array.length; i++) {
            buffer.append(array[i] + "\n");
        }
        return buffer.toString();
    }

}
