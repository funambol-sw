/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.spds;

import java.util.Hashtable;

/**
 * This is a wrapper to create a istance of SyncManagerImpl
 *
 */
public class SyncManager {

    //---------------------------------------------------------------- Constants

    private static final String KEY_LOGIN        = "login"       ;
    private static final String KEY_SERVER_URL   = "serverUrl"   ;
    final private static String KEY_GATEWAYAPN   = "gatewayApn"  ;
    final private static String KEY_GATEWAYIP    = "gatewayIp"   ;
    final private static String KEY_GATEWAYPORT  = "gatewayPort" ;

    //---------------------------------------------------------------- Private data

    SyncManagerImpl syncManagerImpl = null;

    //---------------------------------------------------------------- Constructors

    /**
     * Creates a SyncManager.
     * The datastore is identified by the dataStoreName parameter
     *
     * @param dataStoreName The dataStore to Sync
     *
     */
    public SyncManager (String dataStoreName,
                        String sourceType,
                        String sourceName,
                        Hashtable properties) {

        this.syncManagerImpl = new SyncManagerImpl(dataStoreName, sourceType, sourceName);

        this.syncManagerImpl.setLogin       ((String) properties.get(KEY_LOGIN       ) );
        this.syncManagerImpl.setServerUrl   ((String) properties.get(KEY_SERVER_URL  ) );
        this.syncManagerImpl.setGatewayApn  ((String) properties.get(KEY_GATEWAYAPN  ) );
        this.syncManagerImpl.setGatewayIp   ((String) properties.get(KEY_GATEWAYIP   ) );
        this.syncManagerImpl.setGatewayPort ((String) properties.get(KEY_GATEWAYPORT ) );
        
        if (properties.get("SESSION") != null)
            this.syncManagerImpl.setSessionId ((String) properties.get("SESSION" ) );
        
        /*
         * actually the alertCode field in the SyncManagerImpl
         * is set with this value (if present), but it is never
         * used (it's always substituted with local variables
         * of type int, valorized with values coming from the
         * response message)
         */
        if (properties.get("ALERTCODE") != null)     
            this.syncManagerImpl.setAlertCode ((String) properties.get("ALERTCODE" ) );
    }

    //----------------------------------------------------------- Public methods
    /**
     * Synchronize data.
     * @throws SyncException if an error occurs during synchronization
     * @throws AuthenticationException if the server responded with "non authorized" return code
     */
    public void sync() throws SyncException, AuthenticationException, UpdateException ,Exception{

        this.syncManagerImpl.sync();

    }

}
