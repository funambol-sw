/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry;

import java.util.Hashtable;
import javax.microedition.pim.Event;

import com.funambol.syncclient.sps.DataAccessException;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.util.Persistable;



/**
 * This class implements a cache for the information related to
 * an event, to workaround the RIM bugs in the All Day Event 
 * handling.
 * The events coming from the server are saved in the cache, and
 * a check is done for every event sent back to the server to know
 * if the end date must be adjusted. See XMLEventParser for datails.
 */

public class EventCache {
    // Hash of com.funambol.syncclient.blackberry.EventCache
    private static long KEY = 0xcc565a75eda511L ;

    private static class CacheData extends Hashtable implements Persistable {
        // To remove the cache data upon uninstallation
    };

    private CacheData cache;
    private PersistentObject pobj;

    /**
     * Creates a new cache object taking data from the PersistentStore.
     */
    public EventCache() throws DataAccessException {
        try {
            pobj = PersistentStore.getPersistentObject(KEY);    
            cache = (CacheData)pobj.getContents();
        }
        catch (Exception e){
            StaticDataHelper.log("EventCache(): " + e.toString());
            throw new DataAccessException("error accessing cache.");
        }
        
        if(cache == null){
            StaticDataHelper.log("EventCache: cache empty, initialize it.");
            cache = new CacheData();
        }
        else {
            StaticDataHelper.log("EventCache: cache data loaded from store.");
        }
    }

    public void save() throws DataAccessException {
        try {
            pobj.setContents(cache);
            pobj.commit();
            StaticDataHelper.log("EventCache::save: cache data saved");
        }
        catch (Exception e){
            StaticDataHelper.log("EventCache.save(): " + e.toString());
            throw new DataAccessException("error saving cache.");    //XXX exception?
        }
    }

    public Hashtable get(String id) {
        return (Hashtable)cache.get(id);
    }

    public Hashtable put(String id, Hashtable data) {
        return (Hashtable)cache.put(id, data);
    }

}

