/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */ 
package com.funambol.syncclient.blackberry.email.impl;

import java.util.Vector;
/**
 * <code>StringUtil</code> Utility string class
 *
 */
public class StringUtil {
    
    private Vector stringList=new Vector();

    
    /**
     * Splits the string based on regex 
     * @param String: s
     * @param String: regex
     * @return String []
     */
    public String[] split(String s,String regex){
    
        int index = s.indexOf(regex);
        
        if(index != -1){
            stringList.addElement(s.substring(0,index));
            String tempStr = s.substring(index + regex.length(),s.length());
            split(tempStr,regex);
        }
        else{
            stringList.addElement(s);
        }
        return getStringArray(stringList);
    }

    /**
     * Returns the string array
     * @param Vector: stringList
     * @return String []
     */
    private String[] getStringArray(Vector stringList){
        
        if(stringList==null){
            return null;
        }
        
        String[] stringArray=new String[stringList.size()];
        for(int i=0;i<stringList.size();i++){
            stringArray[i]=(String) stringList.elementAt(i);
        }
        return stringArray;
    }
    
    /**
     * Find two consecutive newlines in a string.
     * @param String: s - The string to search
     * @return int: the position of the empty line
     */
    public static int findEmptyLine(String s){
        int ret = 0; 
        
        // Find a newline
        while( (ret = s.indexOf("\n", ret)) != -1 ){
            // Skip carriage returns, if any
            while(s.charAt(ret) == '\r'){
                ret++;
            }
            if(s.charAt(ret) == '\n'){
                // Okay, it was empty
                ret ++;
                break;
            }
        }
        return ret;
    }
    
    /**
     * Removes unwanted wide spaces
     * @param String: content
     * @return String
     */
    public static String removeWideSpaces(String content){

        if(content==null){              
            return null;
        }

        StringBuffer buff = new StringBuffer();
        buff.append(content);

        for(int i = buff.length()-1; i >= 0;i--){
            if(' ' == buff.charAt(i)){
                buff.deleteCharAt(i);
            }
        }
        return buff.toString();
    }

}

