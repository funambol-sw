/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.email.impl;

import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;


/**
 * Class for storing and retrieving message id's.
 *
 */
public class MessageIDStore {
    
    private long KEY;
    private static PersistentObject persist;
    public MessageIDStore() { 
        KEY = Constants.FOLDERITEMS;
    }
    
   /**
    * sets the passed key for store 
    */    
    public void setMessageMappingKey() {
        this.KEY=Constants.MSGMAPKEY;
    }   
    
   /**
    * Adds new servId & clientId to store 
    * @param   servId Passed server id 
    * @param   clientId Passed client id
    */      
    public void add(String servId, String clientId) {
    
        if (servId == null || clientId == null) {
            return;
        }
        
        String data = servId.trim() + ":" + clientId.trim();
        
        if (checkIsDataThere(data)) { 
            return;
        }
        
        persist = PersistentStore.getPersistentObject(KEY);
        
        String valuesFromStore[] = (String[])persist.getContents();
                
        if (valuesFromStore == null) {
            
            valuesFromStore = new String[1];
            valuesFromStore[0] = new String(data);
            persist.setContents(valuesFromStore);
            persist.commit();
            return;
        
        } else {
            
            String IncrementedStore []= new String[valuesFromStore.length+1];
            
            int i = 0;
            
            for (i = 0; i < valuesFromStore.length;i++) {     
                IncrementedStore[i]=valuesFromStore[i];
            }
            
            IncrementedStore[i]=data;
            
            persist.setContents(IncrementedStore);
            
            persist.commit();
        } 
    }       
  
   
   /**
    * finds out if data is present in Store 
    * @param data The data to be searched 
    * @return true if data is present, else false
    */  
    public boolean checkIsDataThere(String data){
        
        boolean isThere = false;
        persist = PersistentStore.getPersistentObject(KEY);
        String valuesFromStore[] = (String[])persist.getContents();
        
        if (valuesFromStore != null) {
        
            for (int i = 0; i < valuesFromStore.length; i++) {
                
                String[] compare = new StringUtil().split(valuesFromStore[i], ":");
                
                if ((compare != null) && (compare[0].equals(data) || compare[1].equals(data))) {
                    return true;    
                }        
            }
        }
        return isThere;
    }
    
   /**
    * finds if server id  is present for passed clientId in Store 
    * @param String  :clientID  this the passed client id
    * @return String :serverId if it is present else null
    */
    public String getServerId(String clientID) {
    
        String serverId = null;
        persist = PersistentStore.getPersistentObject( KEY );
        String[] valuesFromStore = (String[])persist.getContents();
        if (valuesFromStore != null) {
            for (int i = 0; i < valuesFromStore.length; i++) {
                
                String[] compare = new StringUtil().split(valuesFromStore[i], ":");
                
                if (compare[1].equals(clientID)) {
                    serverId = compare[0];
                    StaticDataHelper.log("[LOG]Found serverId --> " + serverId);               
                }
            }       
        }
           
        return serverId;    
    }
    
    /**
     * finds if client id  is present for passed serverId in Store 
     * @param String  :serverId  this the passed server id
     * @return String :clientID if it is present else null
     */
     public String getClientId(String serverId) {
         
        String clientID = null;
        String temp = serverId;
        persist = PersistentStore.getPersistentObject(KEY);
        String[] valuesFromStore = (String[])persist.getContents();
        if (valuesFromStore != null) {
                        
            for (int i = 0; i < valuesFromStore.length; i++) {
                
                String[] compare = new StringUtil().split(valuesFromStore[i],":");
                
                if (compare[0].equals(serverId)) {
                    clientID =compare[1];
                    StaticDataHelper.log("found clientID-->"+clientID);
                }
            }
        }
           
        return clientID;
    }
   
   
    /**
     * deletes data from Store if present
     * @param data The data to be deleted 
     */       
    public void delete(String data) {
        
        StaticDataHelper.log("[LOG]Data to be deleted: " + data);
        
        persist = PersistentStore.getPersistentObject(KEY);
    
        String[] valuesFromStore = (String[])persist.getContents();
    
        if (valuesFromStore == null) {
            return;    
        } else {
            boolean flag = false;
    
            int j = 0;
    
            String[] small = new String[valuesFromStore.length];
    
            for (int i = 0; i < valuesFromStore.length; i++) {
    
                String[] compare = new StringUtil().split(valuesFromStore[i], ":");
    
                if (compare != null && !compare[0].equals(data)) {
                    small[j++] = valuesFromStore[i];
                } else {
                    flag = true;//found
                }
            }
            
            if (flag) {
                
                if (j > 0) {
                   
                    String[] result = new String[j];
                    
                    for (int k = 0; k < j; k++) {
                        
                        result[k] = small[k];
                        
                        StaticDataHelper.log("[LOG]result --> " + result[k]);
                    }
            
                    persist.setContents(result);
                    persist.commit();
                    
                } else {                    
                    PersistentStore.destroyPersistentObject(KEY);                
                }
                
            } else {  
                persist.setContents(small);
                persist.commit();
            }        
        }
    }
} 

