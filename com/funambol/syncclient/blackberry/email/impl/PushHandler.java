/*
 * Copyright (C) 2003-2006 Funambol
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

package com.funambol.syncclient.blackberry.email.impl;

import java.io.*;
import java.util.Hashtable;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.StreamConnection;

import com.funambol.syncclient.blackberry.Configuration;
import com.funambol.syncclient.blackberry.SyncClient;
import com.funambol.syncclient.blackberry.email.impl.SyncInfo;
import com.funambol.syncclient.spds.NetworkException;
import com.funambol.syncclient.spds.SyncException;
import com.funambol.syncclient.spds.SyncMLClientImpl;
import com.funambol.syncclient.spds.SyncManagerFactory;
import com.funambol.syncclient.sps.DataAccessException;
import com.funambol.syncclient.sps.DataStore;
import com.funambol.syncclient.sps.MailDataStore;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.RadioInfo;






/**
 * Gets notification data and calls parser
 *
 */ 
public class PushHandler {
        
    private static String URL;
    private boolean signal = true;
    private InputStream inStream;
    private OutputStream outStream;
    private String alertCode;
    private String sessionId;
    private static StaticDataHelper sdh;
    private static PersistentObject store;
    private String serverUrl;
    
    final private static String MAIL_SOURCE_NAME         = "mail"                ;
    final private static String MAIL_SOURCE_TYPE         = "application/vnd.omads-email+xml";
    final private static String PROP_SERVER_URL          = "serverUrl";
    final private static String PROP_LOGIN               = "login";
    final private static String PROP_GATEWAYAPN          = "gatewayApn";
    final private static String PROP_GATEWAYIP           = "gatewayIp";
    final private static String PROP_GATEWAYPORT         = "gatewayPort";
    final private static String PROP_SESSION             = "SESSION";
    final private static String PROP_ALERTCODE           = "ALERTCODE";
    
    private static final int
        TWO_WAY_BY_SERVER                                = 206;
    private static final int
        ONE_WAY_FROM_CLIENT_BY_SERVER                    = 207;
   
    private static final int
        REFRESH_FROM_CLIENT_BY_SERVER                    = 208;
    private static final int
        ONE_WAY_FROM_SERVER_BY_SERVER                    = 209;   
    private static final int
        REFRESH_FROM_SERVER_BY_SERVER                    = 210;
   
    private static final String
        TWO_WAY_SYNC                                     = "200"; 
    private static final String
        ALERT_CODE_SLOW                                  = "201";
    private static final String
        ONE_WAY_FROM_CLIENT                              = "202";
    private static final String
        REFRESH_FROM_CLIENT                              = "203";
    private final static String
        ALERT_CODE_ONE_WAY_FROM_SERVER                   = "204";
    private static final String
        ALERT_CODE_REFRESH_FROM_SERVER                   = "205";
        
    final private static String ERROR_NOT_AUTHORIZED     = "401";
    final private static String ERROR_NOT_FOUND          = "404";
    final private static String ERROR_SERVER             = "511";
    final private static String ERROR_REFRESH_REQUIRED   = "508";
    
    public PushHandler() {
    }
    
    static {
        store = PersistentStore.getPersistentObject(SyncClient.CONFIG_KEY);
    }
    
    /**
     * makes socket connection to a server port listening for notifications
     * @throws NetworkException
     * @throws IOException
     */  
    public void connectToServer() throws NetworkException, Exception {        
           
        String gatewayUrl = "";
        
        Configuration.updateConfigdetails();
        StreamConnection connection = null;
        StringBuffer buffer = new StringBuffer();
        int read = 0;
        int ch = -1;
        
        if (Configuration.GATEWAYAPN != null && Configuration.GATEWAYAPN.length() > 0) {
            
            gatewayUrl = ";WAPGatewayIP=" +
            Configuration.GATEWAYIP +
            ";WAPGatewayAPN=" +
            Configuration.GATEWAYAPN +
            ";WAPGatewayPort=" +
            Configuration.GATEWAYPORT;
        }
        else {
            gatewayUrl = "";
        }
        
        serverUrl = Configuration.URL;
        
        if (!DeviceInfo.isSimulator()) {
            serverUrl = serverUrl + gatewayUrl;
        }
        
        String response = "";

        StaticDataHelper.log("Requesting for push message if any......");

        try {
            response = postRequest("");
        } catch (Exception e) {
            String msg = "Error sending request " + e.getMessage();
            StaticDataHelper.log(msg);
            throw e;           
        }
        
        if (!response.equals("")) {
            parseAndProcess(response);
        }       
     }
        
        
    /**
     * Posts the given message to the url specified by <i>url</i>.
     *
     * @param request the request msg
     * @return the url content as a byte array
     * @throws SyncException in case of network errors
     */
    private String postRequest(String request) throws SyncException {
        try {
            SyncMLClientImpl syncMLClient = new SyncMLClientImpl(serverUrl);

            return syncMLClient.sendMessage(request);

        } catch (NetworkException e) {
            String msg = "Synchronization failed: no network available";
            StaticDataHelper.log(msg);
            throw new SyncException(msg);
        } catch (IOException e) {
            String msg = "Synchronization failed, connection broken";
            StaticDataHelper.log(msg);
            throw new SyncException(msg);
        } catch (Exception e) {
            String msg = "Error sending the request: network error";
            StaticDataHelper.log(msg);
            throw new SyncException(msg);
        } 
    }
    
    /**
     * processes the synchronization
     * @throws Exception
     */
    void synchronizeMail() throws Exception {
         
        String[] contents = null;
        String mailSourceType = MAIL_SOURCE_TYPE;
        String mailSourceName = MAIL_SOURCE_NAME;
        
        synchronized(store) {
            contents = (String[])store.getContents();
            if (contents == null) {  
                return;
            }
        }
            
        final String login             = contents[0] + ":" + contents[1];
        final String serverURL         = contents[2];
        final String mailSourceURI     = contents[5];
        final String contactSourceURI  = contents[6];
        final String calendarSourceURI = contents[7];
        final String gatewayApn        = contents[11];
        final String gatewayIp         = contents[12];
        final String gatewayPort       = contents[13];
        
        
        Hashtable properties = new Hashtable();

        properties.put(PROP_SERVER_URL, serverURL);
        properties.put(PROP_LOGIN, login);
        properties.put(PROP_GATEWAYAPN, gatewayApn);
        properties.put(PROP_GATEWAYIP, gatewayIp);
        properties.put(PROP_GATEWAYPORT, gatewayPort);
        properties.put(PROP_SESSION, sessionId);
        properties.put(PROP_ALERTCODE, alertCode);
       
        sdh = new StaticDataHelper();
        sdh.loadLanguage(this.getClass());

        //
        //Load default properties form jad file
        //
        sdh.loadDefaultValue();
        
        sdh.setCalendarSourceUri(calendarSourceURI);
        sdh.setContactSourceUri(contactSourceURI);
        sdh.setMailSourceUri(mailSourceURI);
        
        sdh.setOS();
        
        Configuration.updateConfigdetails();  
             
        // ---------------------------------------------------
        SyncManagerFactory.getSyncManager(mailSourceURI,
                                          mailSourceType,
                                          mailSourceName,
                                          properties).sync();
        // ---------------------------------------------------
    }
        
    /**
     * parses notification data  and calls process to sync
     * @param String:  data
     */        
    public  void parseAndProcess(String data) {
        Integer numSync = null;
        SyncInfo syncInfo = null;
             
        if (data == null || data.equals("")) {
            StaticDataHelper.log("null data from server!");  
            return;
        }
          
        Hashtable ht = new Hashtable();
          
        new NotificationProcessor().processMessage(ht,data);
          
        if (ht != null) {            
            
            sessionId = (String) ht.get("SessionId");
            numSync = (Integer)ht.get("NumberOfSync");
             
            syncInfo = (SyncInfo)ht.get("syncInfo[" + 0 + "]");   
            int syncType = syncInfo.getSyncType();  
            
            switch (syncType) {
                
                case TWO_WAY_BY_SERVER:
                    alertCode = TWO_WAY_SYNC;
                    break;
                
                case ONE_WAY_FROM_CLIENT_BY_SERVER:
                    alertCode = ONE_WAY_FROM_CLIENT;
                    break;
                
                case REFRESH_FROM_CLIENT_BY_SERVER:
                    alertCode = REFRESH_FROM_CLIENT ;  
                    break;
                
                case ONE_WAY_FROM_SERVER_BY_SERVER:
                    alertCode = ALERT_CODE_ONE_WAY_FROM_SERVER;  
                    break;
                
                case REFRESH_FROM_SERVER_BY_SERVER:
                    alertCode = ALERT_CODE_REFRESH_FROM_SERVER;  
                    break;
                
                default:
                    alertCode = ALERT_CODE_SLOW;  
                    break;
            }
            
            try {   
               synchronizeMail();//polling is actually a sync [09-03-2006 11:40]
            } catch (Exception e) { 
               StaticDataHelper.log("Exception--\n" + getSynchError(e.getMessage(), "mail", sdh.getMailSourceUri()));
            }
        
        }//end if      
    }
      
      
    /**
     * return error message
     * 
     * @param error
     * @param remoteDBType
     * @param sourceUri
     */
    private String getSynchError(String error,
                                 String remoteDBType,
                                 String sourceUri) {

        if (ERROR_NOT_AUTHORIZED.equals(error)) {

            return sdh.getLanguage("not-authorized-user-and-password");

        } else if (ERROR_NOT_FOUND.equals(error)) {

            return sdh.getLanguage("not-found-remote-syncsource") + ": " + sourceUri;

        }  else if (error != null && error.indexOf("Connection refused") != -1) {

            return sdh.getLanguage("error-connecting-to-the-server");

        } else if (error != null && error.indexOf("out-of-memory") != -1) {

            return sdh.getLanguage("out-of-memory");
       
        } else if (error != null && ERROR_SERVER.equals(error)) {
        
            return sdh.getLanguage("internal-server-error");

        } else if (error != null && error.indexOf(ERROR_REFRESH_REQUIRED)>=0) {
  
            try {
                MailDataStore dataStore = (MailDataStore)DataStore.getDataStore(sourceUri);
                dataStore.setLastTimestamp(0l);
            } catch (DataAccessException e) {
                //
            }
            
            return "Refresh-Required";         
        
        } else {

            return error;

        }
    }
} 

