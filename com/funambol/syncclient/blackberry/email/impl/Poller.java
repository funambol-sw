/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.email.impl;

import java.io.*;

import com.funambol.syncclient.blackberry.Configuration;
import com.funambol.syncclient.blackberry.SyncClient;
import com.funambol.syncclient.spds.NetworkException;
import com.funambol.syncclient.util.StaticDataHelper;


import net.rim.device.api.system.Memory;

/**
 * Poller class polling for notifications
 *
 */
public class Poller implements Runnable {
        
    //Now this is here only because of the debug menu
    public static long KEY = 0xbc565a75eda511L;
    
    private SyncClient syncClient = null;
    
    public Poller() { }
    
    public void setSyncClientObj(SyncClient obj) {
        this.syncClient = obj;
    }
    
    public void run() {
                
        while (Configuration.enablePolling) {
                
            if (!SyncClient.inSynchronize) {
                
                try {
                        
                    SyncClient.inSynchronize = true;
                    //PushHandler sh= new PushHandler(); //CHANGE 10-03-2006 19:09
                    
                    syncClient.removeMailListener();
                    
                    //sh.connectToServer(); //CHANGE 10-03-2006 19:09
                    syncClient.synchronizeMail();
                    //syncClient.synchronizeMail(); Only for sending
                
                } catch (NetworkException ne) {
                    StaticDataHelper.log("NetworkException--\n" + ne.toString());
                } catch (IOException ioe) {
                    StaticDataHelper.log("IOException--\n" + ioe.toString());
                } catch (Exception ie) {
                    StaticDataHelper.log("Exception--\n" + ie.toString());
                } finally {
                    syncClient.setMailListener();
                    SyncClient.inSynchronize = false;
                }
            }//if
            
            try {   
                Thread.sleep(Configuration.pollInterval);   
            } catch (InterruptedException ie) {
                StaticDataHelper.log("InterruptedException of the sleeping poller thread--\n" + ie.getMessage()); 
            }
            
        }//while
    }//end of run
}
