/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.email.impl;
import java.io.*;
import net.rim.device.api.crypto.*;
import net.rim.device.api.util.DataBuffer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.funambol.syncclient.util.StaticDataHelper;
/**
 * <code>Sync4jEncrypt</code> Parses the MailContent & Fills the mail obj.
 *
 */
 
class Sync4jEncrypt {
    TripleDESKey key;
  
    
    Sync4jEncrypt() {    
     key = new TripleDESKey();
         
    }
    public byte[] encryptData(String data){
        try{
            TripleDESEncryptorEngine encryptionEngine = new TripleDESEncryptorEngine( key );
            PKCS5FormatterEngine formatterEngine = new PKCS5FormatterEngine( encryptionEngine );
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BlockEncryptor encryptor = new BlockEncryptor( formatterEngine, outputStream );
            // Encrypt the actual data.
            encryptor.write( data.getBytes() );
            encryptor.close();
            return outputStream.toByteArray();
        }catch( CryptoTokenException e ) {
            StaticDataHelper.log(e.toString());
        } catch (CryptoUnsupportedOperationException e) {
            StaticDataHelper.log(e.toString());
        } catch( IOException e ) {
            StaticDataHelper.log(e.toString());
        }
        return null;
    }
    
    public String decryptData(byte[] data){
        try{
            TripleDESDecryptorEngine decryptorEngine = new TripleDESDecryptorEngine( key );
            
            PKCS5UnformatterEngine unformatterEngine = new PKCS5UnformatterEngine( decryptorEngine );
            
            ByteArrayInputStream inputStream = new ByteArrayInputStream( data );
            
            BlockDecryptor decryptor = new BlockDecryptor( unformatterEngine, inputStream );
            
            byte[] temp = new byte[10];
            DataBuffer db = new DataBuffer();
            for( ;; ) {
                int bytesRead = decryptor.read( temp );
                if( bytesRead <= 0 ) {
                    break;
                }
                db.write(temp, 0, bytesRead);
            }
            
            byte[] decryptedData = db.toArray();
            return decryptedData.toString();
        }catch( CryptoTokenException e ) {
            StaticDataHelper.log(e.toString());
        } catch (CryptoUnsupportedOperationException e) {
            StaticDataHelper.log(e.toString());
        } catch( IOException e ) {
            StaticDataHelper.log(e.toString());
        }
        return null;
   
    }
    
} 
