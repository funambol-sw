/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.email.impl;

import java.util.Hashtable;
import java.util.Vector;

import com.funambol.syncclient.util.StaticDataHelper;

/**
 * <code>MailContent</code> Parses the MailContent and Fills the mail obj.
 *
 */

public class MailContent 
{
    //-------------------------------------------------------------- Private data
    private String contents;
    private MailBody mailBody = new MailBody();
    private Vector attachmentList=new Vector();
    public static final String CONTENTDISPOSITION = "Content-Disposition";
    public static final String CONTENTTYPE="CONTENT-TYPE";
    public static final String CONTENTTRANSFERENCODING = "Content-Transfer-Encoding";
    public static final String  FILENAME = "filename";
    public static final String  CHARSET = "charset";

    /**
     * This is a constructor for MailContent class.
     * This take MIME message as an input and parses the MIME message.
     * 
     */
    public MailContent(String contents){
        this.contents=contents;
    }

    /**
     * 
     * Returns MailBody object
     * 
     */
    public MailBody getMailBody(){
        return mailBody;
    }

    /**
     * 
     * Returns Vector of MailAttachment object
     * 
     */  
    public Vector getAttachmentList(){
        return attachmentList;
    }

    /**
     * 
     * This method parses the MIME data
     * 
     */  
    public void parseContent(){

        int startIndex         =   0   ;
        int endIndex            =   0   ;
        String contentType      =   ""  ;
        String boundary         =   ""  ;
        String stringHelper             ;

        if (contents==null || contents.equals("*/*\r\n")){
            return;
        }

        String strHelper = contents.toLowerCase();

        endIndex = strHelper.indexOf(";", startIndex);

        if (endIndex > 0){
            contentType = strHelper.substring(startIndex, endIndex).trim();
        }

        if (contentType.equalsIgnoreCase("multipart/related") 
                 || contentType.equalsIgnoreCase("multipart/alternative") 
                 || contentType.equalsIgnoreCase("multipart/mixed")) {

            //Attachments and with body
            boundary="--"+MIMEParser.findBoundary(contents).trim();
            String[] contentsList = new StringUtil().split(contents,boundary);
            for(int i=1; i<contentsList.length-1; i++) {
                processMultiPart(contentsList[i]);
            }
        }
        else{
            //Direct mail body
            try{

                String[] strList= new StringUtil().split(contents,";");

                if(strList==null || strList.length==0){
                    return;
                }

                mailBody.setContentType(strList[0]);

                if((startIndex=contents.indexOf("charset="))>-1){

                    endIndex=contents.indexOf("\r",startIndex);
                    if(endIndex!=-1){
                        mailBody.setCharSet(contents.substring(startIndex+"charset=".length(),endIndex));
                    }
                }

                strList= new StringUtil().split(contents.substring(endIndex,contents.length()),"\n");

                StringBuffer buff=new StringBuffer("");

                int strIndex;

                for(strIndex=0;strIndex<strList.length;strIndex++) {

                    if( StringUtil.removeWideSpaces(strList[strIndex]).equals("\r") 
                            || StringUtil.removeWideSpaces(strList[strIndex]).equals("\r\r")){

                        break;
                    }
                }

                for(;strIndex<strList.length;strIndex++) {
                    buff.append(strList[strIndex]+"\n");
                }

                mailBody.setActualContent(buff.toString());

            }
            catch(Exception e){
                StaticDataHelper.log(e.getMessage());
            }

        }
    }

    /**
     * 
     * This method processes the Multipart data
     * 
     */  

    private void processMultiPart(String multiPart) {

        Hashtable ht = new Hashtable();

        try {

            ht = new MIMEParser().CreateHashTable(multiPart);

            // Attachments have the Content-Disposition header. Can be either
            // inline or attachment, they are attachment anyway.
            String value = (String)ht.get(CONTENTDISPOSITION.toUpperCase());

            if( value != null ){

                //Attachment..
                MailAttachment objAttachment = new MailAttachment();

                objAttachment.setContentDisposition(value.trim());

                if(ht.get(CONTENTTRANSFERENCODING.toUpperCase()) != null){

                    objAttachment.setContentTransferEncoding(((String)
                                ht.get(CONTENTTRANSFERENCODING.toUpperCase())).trim());
                }

                if(ht.get(FILENAME.toUpperCase()) != null){

                    objAttachment.setFileName(((String)
                                ht.get(FILENAME.toUpperCase())).trim());
                }

                String content = (String) ht.get(CONTENTTYPE);
                int contentIndex = content.indexOf(";");
                String contentHeader = null;
                String actualContent = null;

                if(contentIndex != -1){
                    contentHeader = content.substring(0, contentIndex);
                    // Jump to the end of the headers.
                    // TODO: what about the others?
                    // FIXME: CODE REPLICATION
                    contentIndex = StringUtil.findEmptyLine(content)+1;
                    actualContent = content.substring(contentIndex, content.length());
                    objAttachment.setContentType(contentHeader.trim());
                    objAttachment.setactualContent(actualContent.trim());

                }

                attachmentList.addElement(objAttachment);
            }
            else{
                //Body of mail..
                //if string has only body & its related header
                if(MIMEParser.findBoundary(multiPart) == null){
                    ht = new MIMEParser().CreateHashTable(multiPart);

                    if(ht.get(CONTENTTRANSFERENCODING.toUpperCase()) != null){
                        mailBody.setContentTransferEncoding(((String)
                                    ht.get(CONTENTTRANSFERENCODING.toUpperCase())).trim());
                    }
                    if(ht.get(CHARSET.toUpperCase()) != null){
                        mailBody.setCharSet(((String)ht.get(CHARSET.toUpperCase())).trim());
                    }

                    String content = (String) ht.get(CONTENTTYPE);
                    int contentIndex = content.indexOf(";");
                    String contentHeader = null;
                    String actualContent = null;

                    if(contentIndex != -1){
                        contentHeader = content.substring(0, contentIndex);
                        // Jump to the end of the headers.
                        // TODO: what about the others?
                        // FIXME: CODE REPLICATION
                        contentIndex = StringUtil.findEmptyLine(content)+1;
                        actualContent = content.substring(contentIndex, content.length());
                        mailBody.setContentType(contentHeader.trim());
                        mailBody.setActualContent(actualContent.trim());

                    }
                }//if string has only body & its related header
                else{

                    String boundary="--"+MIMEParser.findBoundary(multiPart).trim();
                    String[] contentsList= new StringUtil().split(multiPart,boundary);

                    if(contentsList==null || contentsList.length==0){
                        return;
                    }

                    for(int i=1;i<contentsList.length-1;i++){
                        ht = new MIMEParser().CreateHashTable(contentsList[i]);
                        if(ht.get(CONTENTTRANSFERENCODING.toUpperCase()) != null){
                            mailBody.setContentTransferEncoding(((String)
                                        ht.get(CONTENTTRANSFERENCODING.toUpperCase())).trim());
                        }
                        if(ht.get(CHARSET.toUpperCase()) != null){
                            mailBody.setCharSet(((String)ht.get(CHARSET.toUpperCase())).trim());
                        }

                        String content = (String) ht.get(CONTENTTYPE);
                        int contentIndex = content.indexOf(";");
                        String contentHeader = null;
                        String actualContent = null;

                        if(contentIndex != -1){
                            contentHeader = content.substring(0,contentIndex);
                            actualContent = content.substring(contentIndex+1,content.length());
                            mailBody.setContentType(contentHeader.trim());
                            mailBody.setActualContent(actualContent.trim());
                        }

                        if(contentHeader.trim().indexOf("text/plain")!=-1 ){
                            break;
                        }

                    }
                }
            }//end of else
        } catch (Exception e){
            StaticDataHelper.log(e.getMessage());
        }
    }
}

class MailBody{
    private String contentType;
    private String charSet;
    private String contentTransferEncoding;
    private String actualContent;

    /**
     * @return Returns the actualContent.
     */
    public String getActualContent() {
        return actualContent;
    }
    /**
     * @param actualContent The actualContent to set.
     */
    public void setActualContent(String actualContent) {
        this.actualContent = actualContent;
    }
    /**
     * @return Returns the charSet.
     */
    public String getCharSet() {
        return charSet;
    }
    /**
     * @param charSet The charSet to set.
     */
    public void setCharSet(String charSet){    
        this.charSet = charSet;
    }
    /**
     * @return Returns the contentTransferEncoding.
     */
    public String getContentTransferEncoding(){
        return contentTransferEncoding;
    }
    /**
     * @param contentTransferEncoding The contentTransferEncoding to set.
     */
    public void setContentTransferEncoding(String contentTransferEncoding) {
        this.contentTransferEncoding = contentTransferEncoding;
    }
    /**
     * @return Returns the contentType.
     */
    public String getContentType() {
        return contentType;
    }
    /**
     * @param contentType The contentType to set.
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}

class MailAttachment
{
    private String contentType;
    private String contentTransferEncoding;
    private String fileName;
    private String contentDisposition;
    private String actualContent;

    /**
     * @return Returns the actualContent.
     */           
    public String getactualContent() {
        return actualContent;
    }

    public void setactualContent(String content) {
        this.actualContent = content;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public String getContentTransferEncoding() {
        return contentTransferEncoding;
    }

    public void setContentTransferEncoding(String contentTransferEncoding) {
        this.contentTransferEncoding = contentTransferEncoding;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }      
}

