/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.email.impl;
import java.io.*;
import net.rim.device.api.crypto.*;
import net.rim.device.api.util.DataBuffer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.crypto.MD5Digest; 
/**
 * <code>Sync4jCrypto</code> Uses TripleDES Encryption & Decryption to encrypt & decrypt the data
 *
 */
 
public class Sync4jDesCrypto {
     DESKey key;
     private byte [] cred;
    
     public Sync4jDesCrypto(byte [] cred ) {  
     this.cred = cred;  
     MD5Digest md = new MD5Digest();
     md.update(cred);
     byte[] keyBytes = md.getDigest(true);
     key = new DESKey(keyBytes);
     StaticDataHelper.log(""+key);    
    }
    
     /**
    *@param String: data
    *encrypts  the data  
    *
    **/
    public byte[] encryptData(String data){
        try{
            StaticDataHelper.log(""+key);
            DESEncryptorEngine encryptionEngine = new DESEncryptorEngine( key );
            PKCS5FormatterEngine formatterEngine = new PKCS5FormatterEngine( encryptionEngine );
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BlockEncryptor encryptor = new BlockEncryptor( formatterEngine, outputStream );
            // Encrypt the actual data.
            encryptor.write( data.getBytes() );
            encryptor.close();
            return outputStream.toByteArray();
        }catch( CryptoTokenException e ) {
            StaticDataHelper.log(e.toString());
        } catch (CryptoUnsupportedOperationException e) {
            StaticDataHelper.log(e.toString());
        } catch( IOException e ) {
            StaticDataHelper.log(e.toString());
        }
        return null;
    }
    
    /**
    *@param byte [] : data
    *@return String : data 
    *decrypts  the data  
    *
    **/
    
    public String decryptData(byte[] data){
        try{
            
            StaticDataHelper.log(""+key);
            DESDecryptorEngine decryptorEngine = new DESDecryptorEngine( key );
            
            PKCS5UnformatterEngine unformatterEngine = new PKCS5UnformatterEngine( decryptorEngine );
            
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            
            BlockDecryptor decryptor = new BlockDecryptor( unformatterEngine, inputStream );
            
            byte[] read = new byte[data.length];
            DataBuffer databuffer = new DataBuffer();
         
            int bytesRead = decryptor.read( read );
            if( bytesRead > 0 ) {
            databuffer.write(read, 0, bytesRead);
            }
            
            byte[] decryptedData = databuffer.toArray();
            return new String(decryptedData);
        }catch( CryptoTokenException e ) {
            StaticDataHelper.log(e.toString());
        } catch (CryptoUnsupportedOperationException e) {
            StaticDataHelper.log(e.toString());
        } catch( IOException e ) {
          
         
            StaticDataHelper.log(e.toString());
        }
        return null;
   
    }
    
} 
