/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry;

import java.util.Vector;
import net.rim.device.api.util.Persistable;

/**
 * Re-implements the java.util.Vector class (suitable to store objects
 * in the device's permanent store) but belongs to the same package as
 * the application (SyncClient), so that removing the latter, all its
 * configuration data are also removed when stored as StringVector objects
 */
public class StringVector extends Vector implements Persistable
{
    /**
     * The constructor invokes the constructor of the superclass,
     * because we need just a Vector but implemented in the same 
     * package as the application that can get removed. All application
     * data stored in objects of this type are then automatically
     * removed from the device's persistent store when the user uninstall
     * the application with the BlackBerry Desktop Manager. Objects
     * in the persistent store of type @link{java.util.Vector} or
     * of array type (e.g. String[]) are let in the storage because
     * they can be virtually referenced by other general applications
     * on the device
     */
    public StringVector()
    {    
        super();
    }
}
