/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.listener;

import java.io.IOException;
import java.util.Hashtable;

import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.io.DatagramConnection;

import com.funambol.syncclient.blackberry.Configuration;
import com.funambol.syncclient.blackberry.SyncClient;
import com.funambol.syncclient.blackberry.email.impl.NotificationProcessor;
import com.funambol.syncclient.blackberry.email.impl.SyncInfo;
import com.funambol.syncclient.sps.DataAccessException;
import com.funambol.syncclient.sps.DataStore;
import com.funambol.syncclient.sps.MailDataStore;
import com.funambol.syncclient.sps.NetworkException;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.ui.component.Dialog;



/**
 * Listener to detect incoming SMS
 * 
 */
public class SMSListener implements Runnable {
    
    private static final int TWO_WAY_BY_SERVER             = 206;
    private static final int ONE_WAY_FROM_CLIENT_BY_SERVER = 207;
    private static final int REFRESH_FROM_CLIENT_BY_SERVER = 208;
    private static final int ONE_WAY_FROM_SERVER_BY_SERVER = 209;   
    private static final int REFRESH_FROM_SERVER_BY_SERVER = 210;
   
    private static final String TWO_WAY_SYNC                   = "200"; 
    private static final String ALERT_CODE_SLOW                = "201";
    private static final String ONE_WAY_FROM_CLIENT            = "202";
    private static final String REFRESH_FROM_CLIENT            = "203";
    private static final String ALERT_CODE_ONE_WAY_FROM_SERVER = "204";
    private static final String ALERT_CODE_REFRESH_FROM_SERVER = "205";
    
    private static final String ERROR_NOT_AUTHORIZED   = "401";
    private static final String ERROR_NOT_FOUND        = "404";
    private static final String ERROR_SERVER           = "511";
    private static final String ERROR_REFRESH_REQUIRED = "508";
    
    /*
     * this is the same for the SMS
     * listener and the scheduled poller,
     * but isn't used anymore (the
     * poller's one was mantained
     * only in order to let a debug
     * menu item compiling)
     */
    public static long KEY = 0xbc565a75eda511L;
    
    String alertCode = null;
    String sessionId = null;
    Integer numSync = null;
    SyncInfo syncInfo = null;
    
    StaticDataHelper sdh = new StaticDataHelper();
    
    /**
     * This is to be used together with the
     * {@link #setSyncClient()} method, to make
     * available here in this class an instance
     * of the application (from the application
     * itself!)
     */
    private SyncClient syncClient = null;  
    
    private boolean stop = false;
    private DatagramConnection dc;
    
    /**
     * To stop immediately an SMS listener, when
     * the configuration is modified by the user
     * 
     */
    public synchronized void stop() {
        
        Dialog.alert("SMS Server Alerted Sync disabled");
        
        stop = true;
        
        try {
            
            /*
             * closes the connection,
             * and the thread returns
             * (i.e., the run() method
             * is exited) because
             * 'stop' == 'true'
             */
            dc.close();
        } catch (IOException e) {
            StaticDataHelper.log("[SMS] EXCEPTION by closing the datagram connection: " + e.toString());
        }
    }

    public void run() {
        try {
            //open a standard SMS connection
            dc = (DatagramConnection)Connector.open("sms://");
            
            while (true) {
                
                if (stop) {
                    
                    StaticDataHelper.log("[SMS] Returning from run() while 'stop' is " + stop);
                    return;
                }
                
                Datagram d = dc.newDatagram(dc.getMaximumLength());
                
                //
                dc.receive(d);
                //
                
                String address = new String(d.getAddress());
                StaticDataHelper.log("[SMS] Message received: ");
                StaticDataHelper.log("[SMS] From: " + address);
                
                /*
                 * The NotificationProcessor reads the
                 * binary SMS notification message and extracts
                 * informations from it that are stored into a
                 * passed hashtable
                 */
                NotificationProcessor processor = new NotificationProcessor();
                
                /*
                 * The hashtable to be passed to the
                 * notification processor to be filled
                 * with data from the binary SMS from
                 * the server. See
                 * NotificationProcessor.processMessage
                 */
                Hashtable smsData = new Hashtable();
                
                //getData() returns an array of bytes
                processor.processMessage(smsData, removeHeader(d.getData()));
                
                if (smsData != null) {
                    
                    StaticDataHelper.log("[SMS] Data from SMS message:\n" + smsData.toString());
            
                    sessionId = (String)smsData.get("SessionId");
                    numSync = (Integer)smsData.get("NumberOfSync");
                    
                    syncInfo = (SyncInfo)smsData.get("syncInfo[" + 0 + "]");   
                    int syncType = syncInfo.getSyncType();  
                    
                    /*
                     * the field 'alertCode' here isn't
                     * passed in any way to the SyncManager
                     * constructor and therefore it's useless
                     * here. For future uses
                     */
                    switch (syncType) {
                        
                        case TWO_WAY_BY_SERVER:
                            alertCode = TWO_WAY_SYNC;
                            break;
                        
                        case ONE_WAY_FROM_CLIENT_BY_SERVER:
                            alertCode = ONE_WAY_FROM_CLIENT;
                            break;
                        
                        case REFRESH_FROM_CLIENT_BY_SERVER:
                            alertCode = REFRESH_FROM_CLIENT ;  
                            break;
                        
                        case ONE_WAY_FROM_SERVER_BY_SERVER:
                            alertCode = ALERT_CODE_ONE_WAY_FROM_SERVER;  
                            break;
                        
                        case REFRESH_FROM_SERVER_BY_SERVER:
                            alertCode = ALERT_CODE_REFRESH_FROM_SERVER;  
                            break;
                        
                        default:
                            alertCode = ALERT_CODE_SLOW;  
                            break;
                        }
                    
                    try {
                        
                        // --------------------------
                        if (Configuration.syncMail) {
                                                
                            sdh.log("[SMS]Testing inSynchronize: " + SyncClient.inSynchronize);
            
                            if (!SyncClient.inSynchronize) {
                                
                                sdh.log("[SMS]if entered, because inSynchronize is " + SyncClient.inSynchronize);
                
                                try {
                        
                                    SyncClient.inSynchronize = true;
                                    
                                    //to avoid collisions with the mail folder listener
                                    syncClient.removeMailListener();
                        
                                    syncClient.synchronizeMail();
                                    //syncClient.synchronizeMail(); Only for sending
                                
                                } catch (NetworkException ne) {
                                        StaticDataHelper.log("[SMS]Exception in synchronization block: " + ne.toString());
                                } catch (IOException ioe) {
                                        StaticDataHelper.log("[SMS]Exception in synchronization block: " + ioe.toString());
                                } catch (Exception ie) {
                                        StaticDataHelper.log("[SMS]Exception in synchronization block: " + ie.toString());
                                } finally {
                                    SyncClient.inSynchronize = false;
                                    syncClient.setMailListener();
                                }
                            }//if  
                        }//if
                    } catch (Exception e) {
                        StaticDataHelper.log("[SMS]Exception in SMSListener.run(): " 
                            + getSynchError(e.getMessage(), "mail", Configuration.mailSourceUri));
                        StaticDataHelper.log("[SMS]Exception in SMSListener.run(): "
                            + e.toString());//better logging than here above
                    }
                }//end if      
            }//end while loop
        } catch (IllegalArgumentException i) {
            StaticDataHelper.log("[SMS]EXCEPTION by opening the connection: " + i.toString());
        } catch (ConnectionNotFoundException c) {
            StaticDataHelper.log("[SMS]EXCEPTION by finding the connection: " + c.toString());
        } catch (IOException e) {
            StaticDataHelper.log("[SMS]EXCEPTION because the SMS thread was stopped or a firewall blocked the connection: " + e.toString());
        }
    }//end run()
    
    /**
     * Returns error messages
     * 
     * @param error
     * @param remoteDBType
     * @param sourceUri
     */
    private String getSynchError(String error,
                                 String remoteDBType,
                                 String sourceUri) {

        if (ERROR_NOT_AUTHORIZED.equals(error)) {

            return sdh.getLanguage("not-authorized-user-and-password");

        } else if (ERROR_NOT_FOUND.equals(error)) {

            return sdh.getLanguage("not-found-remote-syncsource") + ": " + sourceUri;

        }  else if (error != null && error.indexOf("Connection refused") != -1) {

            return sdh.getLanguage("error-connecting-to-the-server");

        } else if (error != null && error.indexOf("out-of-memory") != -1) {

            return sdh.getLanguage("out-of-memory");
       
        } else if (error != null && ERROR_SERVER.equals(error)) {
        
            return sdh.getLanguage("internal-server-error");

        } else if (error != null && error.indexOf(ERROR_REFRESH_REQUIRED) >= 0) {
  
            try {
                MailDataStore dataStore = (MailDataStore)DataStore.getDataStore(sourceUri);
                dataStore.setLastTimestamp(0l);
            } catch (DataAccessException e) {
                StaticDataHelper.log("Exception in SMSListener.getSynchError(...) --> " + e.toString());
            }
            
            return "Refresh-Required";         
        
        } else {
            return error;
        }
    }
    
    
    /**
     * Makes available in this class an instance
     * of the application in order to synchronize
     * with methods that aren't static but instance
     * methods of the application itself. To be
     * invoked from within the application
     * ({@link SyncClient})
     * 
     * @param obj The SyncClient's instance passed
     *            by the SyncClient itself to this
     *            class
     * @see SyncClient
     */
    public void setSyncClientObj(SyncClient obj) {
        this.syncClient = obj;
    }
    
    
    
    /**
     * The first 3 bytes + the number of bytes indicated in the 3rd byte
     * of the binary SMS message from server are to be skipped before they
     * are passed to the parser (the {@link NotificationProcessor})
     * @param in The binary SMS message complete with a useless header
     *           as an array of bytes 
     * @return The message as a binary array without the SMS header
     */
    private byte[] removeHeader(byte[] in) {
        int elements = in.length - 6;
        byte[] out = new byte[elements];
        System.arraycopy(in, 6, out, 0, elements);
        return out;
    }
}
