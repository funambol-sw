/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.listener;

import net.rim.blackberry.api.pdap.BlackBerryContact;
//import javax.microedition.pim.Contact;

import javax.microedition.pim.Event;
import javax.microedition.pim.PIMItem;

import com.funambol.syncclient.sps.ContactDataStore;

import net.rim.blackberry.api.pdap.PIMListListener;


/**
 * <code>Sync4jContactListener</code> listens for changes to the BlackBerry
 * contact database and records the changes to a persistent store. The stored
 * information is used for syncing with Funambol subsequently
 *
 */
public class Sync4jContactListener implements PIMListListener{

    //Data store object
    private ContactDataStore contactDataStore ;
   
    /**
     * Default Constructor for Sync4jPIMListener class
     * that initializes the store object.
     *
     * @param void
     *
     */
    public Sync4jContactListener() {
        this.contactDataStore = new ContactDataStore(); 
    }

    /**
     * This method is invoked when the item is added to blackberry addressbook.
     * @param PIMItem ,item that was added to the address book
     * @return void
     */
    public void itemAdded(PIMItem item) {
        storeRecord(item, ContactDataStore.RECORD_STATE_NEW);
    }

    /**
     * This method is invoked when the item is updated in blackberry addressbook.
     * @param PIMItem oldItem, item that was changed from the address book
     * @param PIMItem newItem, item that is changed from the address book
     * @return void
     */
    public void itemUpdated(PIMItem oldItem, PIMItem newItem) {    
        storeRecord(newItem, ContactDataStore.RECORD_STATE_UPDATED);
    }

    /**
     * This method is invoked when an item is removed from blackberry addressbook.
     * @param PIMItem item that was removed from the address book
     * @return void
     */
    public void itemRemoved(PIMItem item) {
        storeRecord(item, ContactDataStore.RECORD_STATE_DELETED);
    }

    /**
     * This method is invoked internally when any changes to the
     * BlackBerry address book happen. It updates the information
     * into persistant store.
     *
     * @param PIMItem Item that was removed from the address book
     */
    private void storeRecord(PIMItem item, char state)
    {
        BlackBerryContact contact = (BlackBerryContact)item;
        String uid = contact.getString(BlackBerryContact.UID, 0);
             
        try
        {
            contactDataStore.addRecord(uid, state, contact);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
