/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry.listener;

import javax.microedition.pim.Contact;
import javax.microedition.pim.Event;
import javax.microedition.pim.PIMItem;

import com.funambol.syncclient.sps.DataStore;
import com.funambol.syncclient.sps.EventDataStore;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.blackberry.api.pdap.PIMListListener;


/**
 * Listens for changes to the BlackBerry contact database and
 * records the changes to a persistent store. The stored
 * information is used for syncing with sync4j subsequently
 *
 */
public class Sync4jEventListener implements PIMListListener
{
    //Data store object
    private EventDataStore eventDataStore;

    /**
     * In the default constructor the store object
     * is initialized
     *
     */
    public Sync4jEventListener()
    {
        this.eventDataStore = new EventDataStore(); 
    }

    /**
     * This method is invoked when an event item is added
     * to the BlackBerry's calendar
     * 
     * @param PIMItem Item that was added to the calendar
     * @return void
     */
    public void itemAdded(PIMItem item)
    {
        /*
         * this code was disabled because of a
         * blackberry bug in managing calendar
         * events
         */
        storeRecord(item, DataStore.RECORD_STATE_NEW);
    }

    /**
     * This method is invoked when an event item is updated
     * in the BlackBerry's calendar
     * 
     * @param PIMItem oldItem, item that was changed from the calendar
     * @param PIMItem newItem, item that is changed from the calendar
     * @return void
     */
    public void itemUpdated(PIMItem oldItem, PIMItem newItem)
    {
        /*
         * this code was disabled because of a
         * blackberry bug in managing calendar
         * events
         */
        storeRecord(newItem, DataStore.RECORD_STATE_UPDATED);
    }

    /**
     * Invoked when a calendar event item is removed from 
     * the BlackBerry's calendar
     * 
     * @param PIMItem Item that was removed from the calendar
     * @return void
     */
    public void itemRemoved(PIMItem item)
    {
        /*
         * this code was disabled because of a
         * blackberry bug in managing calendar
         * events
         */
        storeRecord(item, DataStore.RECORD_STATE_DELETED);
    }

    /**
     * This method is invoked internally when any changes to the
     * BlackBerry calendar happen. It updates the information in
     * the persistent store
     *
     * @param PIMItem Item that was removed from, added to or updated in the calendar
     * @return void
     */
    private void storeRecord(PIMItem item, char state)
    {
        Event event = (Event)item;
        String uid = null;

        try
        {
            /**
             * The unique ID field is valid only if
             * the event has been added to an EventList
             * at least once in the past. If the Event
             * has not yet been added to a list,
             * the UID returns null
             */
            uid = event.getString(Event.UID, 0);
            
            /**
             * We're actually passing the UID String,
             * the state char, the PIM List casted to Event
             * (the later to retrieve the UID from the
             * more generic PIM Item)
             */
            eventDataStore.addRecord(uid, state, event);
        }
        catch (Throwable e)//Exception
        {
            StaticDataHelper.log("EXCEPTION in Sync4jEventListener.storeRecord(PIMItem, char) --> " + e.toString());
            e.printStackTrace();
        }
    }
}
