/*
 * Copyright (C) 2003-2006 Funambol
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.funambol.syncclient.blackberry.listener;

import net.rim.blackberry.api.phone.AbstractPhoneListener;
import net.rim.blackberry.api.phone.Phone;
import net.rim.blackberry.api.phone.PhoneCall;

import net.rim.device.api.ui.component.Status;

/**
 * Listen to an incoming call. If the call number is recognized
 * as the Funambol Sync Server number, then start to sync mails.
 * Only passive calls are handled. The server should send only
 * one impulse. This class has to be registerd as listener in
 * the main application (@see SyncClient)
 * 
 * @see net.rim.blackberry.api.phone.AbstractPhoneListener
 * @see net.rim.blackberry.api.phone.PhoneListener
 */
public class CallsListener extends AbstractPhoneListener
{
    /**
     * We don't need anything special in the constructor
     */
    public CallsListener() {    }
    
    /**
     * Invoked when a new call is arriving
     * @param callId ID for the call
     */
    public void callIncoming(int callId)
    {
        PhoneCall call = Phone.getCall(callId);
        String phoneNumber = call.getDisplayPhoneNumber();
        
        if (phoneNumber.equals("+393402662957"))
        {
            Status.show(">>>>>> Got incoming call from number " + phoneNumber);
            System.out.println(">>>>>> Got incoming call from number " + phoneNumber);
        }
        else return;
    }
} 
