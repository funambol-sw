/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.blackberry;

import java.util.Hashtable;

import javax.microedition.rms.*; 

import com.funambol.syncclient.util.Log;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.Persistable;


/**
 * Configuration class for configuration details
 * 
 */
public class Configuration {
    
    //------------------------------------------------------------ Constants
    // This is the default polling interval
    private static final int INTERVAL = 5;
    
    // This is the default sync interval
    private static final int SYNC_INTERVAL = 2;

    // This field contains a version string of the  configuration data
    // It is the date of the last change made to the conf data.
    public static final String VERSION = "20060721" ;

    // Return values for load()
    public static final int CONF_OK = 0 ;
    public static final int CONF_NOTSET = -1;
    public static final int CONF_INVALID = -2;
    public static final int PARAM_NOTSET = 1;

    //------------------------------------------------------------ Static Members
    public static String RETRY; // XXX: what's this?
    public static String MAXMSGSIZE = "16000";
    public static String PROTOCOL; // XXX: what's this?

    // TODO: make them private with access methods
    // Connection params
    public static String syncUrl           ;
    public static String userName          ;
    public static String password          ;
    // Contacts params
    public static boolean syncContact       ;
    public static String contactSourceUri  ;
    // Calendar params
    public static boolean syncCalendar      ;
    public static String calendarSourceUri ;
    // Mail params
    public static boolean syncMail          ;
    public static String mailSourceUri     ;
    public static String mailAddress       ;
    public static boolean enablePolling     ;
    public static int pollInterval      ;
    public static boolean enableSmsSync     ;
    // krk Auto Sync params
    public static boolean enableAutoSync     ;
    public static int syncInterval     ;
    // Network params
    public static String gatewayApn        ;
    public static String gatewayIp         ;
    public static String gatewayPort       ;
    public static String listeningPort     ;
    
    public static int logLevel = Log.DEBUG  ; // FIXME: use cofig screen
    
    // Inner class for the data stored.
    private static class ConfigData extends Hashtable implements Persistable {
        /**
         * Get the value 'key' from config data and return it
         * if valid. Otherwise, defval is returned
         */
        public String getValue(String key, String defval) {
            String v = (String)get(key);
            if (v != null){
                Log.debug("getValue: key="+key+", val="+v);
                return v;
            }
            else {
                Log.error("Parameter not found: " + key);
                return defval;
            }
        }

        public boolean getValue(String key, boolean defval) {
            String v = (String)get(key);
            if (v != null){
                Log.debug("getValue: key="+key+", val="+v);
                return ("true".equals(v) ? true : false );
            }
            else {
                Log.error("Parameter not found: " + key);
                return defval;
            }
        }
        
        public int getValue(String key, int defval) {
            String v = (String)get(key);
            if (v != null){
                Log.debug("getValue: key="+key+", val="+v);
                return Integer.parseInt(v) ;
            }
            else {
                Log.error("Parameter not found: " + key);
                return defval;
            }
        }
        
        /**
         * Set the value 'key' in the config data
         */
        public void setValue(String key, String val) {
            put(key, val);
        }
        
        public void setValue(String key, boolean val) {
            put(key, (val)?"true":"false");
        }
        
        public void setValue(String key, int val){
            put(key, new Integer(val).toString().trim());
        }
    };

    static {
        copyDefaults();
    }
    
    // This constructor must be called once at the beginning of the 
    // application.
    Configuration(Class c) throws Exception {
        // load the language.xml configuration file
        // in the 'language' string...
        StaticDataHelper sdh = new StaticDataHelper();
        sdh.loadLanguage(c);

        // ... and load the default properties from
        // that file (and **not** from a .jad file)
        // into private static fields of StaticDataHelper
        sdh.loadDefaultValue();

        copyDefaults();
    }
    
    Configuration() {
    }
    
    private static void copyDefaults() {

        StaticDataHelper sdh = new StaticDataHelper();
                    
        syncUrl = sdh.getHomePageDefault();
        userName = sdh.getUserDefault();
        password = sdh.getPasswordDefault();
        
        syncContact  = sdh.getSyncContactDefault() ;
        contactSourceUri = sdh.getContactSourceUriDefault() ;
        
        syncCalendar = sdh.getSyncCalendarDefault();
        calendarSourceUri = sdh.getCalendarSourceUriDefault() ;
        
        syncMail = sdh.getSyncMailDefault();
        mailSourceUri = sdh.getMailSourceUriDefault() ;
        mailAddress = sdh.getEmailIdDefault() ;
        enablePolling = sdh.getEnablePollingDefault();
        
        enableAutoSync = sdh.getEnableAutoSyncDefault();

        // FIXME: hardcoded value in minutes. Change ConfigurationScreen too
        pollInterval = INTERVAL ;
        syncInterval = SYNC_INTERVAL ;
        enableSmsSync  = sdh.getSmsServerAlertedSyncDefault() ;
        
        gatewayApn = sdh.getGatewayApnDefault() ;
        gatewayIp = sdh.getGatewayIpDefault() ;
        gatewayPort = sdh.getGatewayPortDefault() ;

        // XXX: for direct push, not used now
        listeningPort = sdh.getAlertingPortDefault() ;

    }
    
    //------------------------------------------------------------ Public methods

    /**
     * Load the current config from the persistent store.
     *
     * @return:
     * <li><b>CONF_OK</b>: if all the data were present in the store.
     * <li><b>CONF_NOTSET</b>: if the store is not present.
     *     Configuration remains untouched.
     * <li><b>CONF_INVALID</b>: if the store does not contain valid data.
     *     Configuration is reverted to default.
     *     
     * Note: if a parameter is not present in the store, the current value
     * is kept for it.
     */
//     * <li><b>A positive number</b>, representing the number of parameters
//     *     wrong, if the store does not contain one or more values.
//     *     The missing params maintain the current value.
    public static int load() {
        PersistentObject pobj = PersistentStore.getPersistentObject(SyncClient.CONFIG_KEY);
        
        try {
            ConfigData contents = (ConfigData)pobj.getContents();
    
            if (contents == null) {
                Log.info("Configuration not set, using current values.");
                return -1;
            }
    
            if (contents.get("VERSION") == null) {
                Log.error("Configuration invalid: reverting to default.");
                PersistentStore.destroyPersistentObject(SyncClient.CONFIG_KEY);
                copyDefaults();
                return -2;
            }
    
            logLevel = contents.getValue("LOG_LEVEL", logLevel) ; 
            
            syncUrl = contents.getValue("SYNC_URL", syncUrl);
            userName = contents.getValue("USERNAME", userName);
            password = contents.getValue("PASSWORD", password);
    
            syncContact = contents.getValue("SYNC_CARD", syncContact);
            contactSourceUri = contents.getValue("CARD_URI", contactSourceUri);
    
            syncCalendar = contents.getValue("SYNC_CAL", syncCalendar);
            calendarSourceUri = contents.getValue("CAL_URI", calendarSourceUri);
    
            syncMail = contents.getValue("SYNC_MAIL", syncMail) ;
            mailSourceUri = contents.getValue("MAIL_URI", mailSourceUri) ;
            mailAddress = contents.getValue("MAIL_ADDR", mailAddress) ;
            enablePolling = contents.getValue("POLL_ENABLE", enablePolling) ;
            pollInterval = contents.getValue("POLL_TIME", pollInterval) ;
            enableSmsSync = contents.getValue("SMS_ENABLE", enableSmsSync) ;

            enableAutoSync = contents.getValue("SYNC_ENABLE", enableAutoSync) ;
            syncInterval = contents.getValue("SYNC_TIME", syncInterval) ;
    
            gatewayApn = contents.getValue("GW_APN", gatewayApn) ;
            gatewayIp = contents.getValue("GW_IP", gatewayIp) ;
            gatewayPort = contents.getValue("GW_PORT", gatewayPort) ;
            listeningPort = contents.getValue("PUSH_PORT", listeningPort) ; 
    
            return CONF_OK;
        }
        catch (ClassCastException cce){
            Log.error("Configuration invalid: reverting to default.");
            PersistentStore.destroyPersistentObject(SyncClient.CONFIG_KEY);
            copyDefaults();
            return -2;
        }
    }

    /**
     * Save the current config to the persistent store.
     */
    public static int save() {
        PersistentObject pobj = PersistentStore.getPersistentObject(SyncClient.CONFIG_KEY);
        
        ConfigData contents = new ConfigData();

        // This field can be used to check which version of the sw
        // saved the data. If not present, it is assumed it's an
        // old version and the saved config is discarded.
        contents.setValue("VERSION", VERSION);
        
        contents.setValue("SYNC_URL", syncUrl);
        contents.setValue("USERNAME", userName);
        contents.setValue("PASSWORD", password);
        
        contents.setValue("SYNC_CARD", syncContact);
        contents.setValue("CARD_URI", contactSourceUri);
        
        contents.setValue("SYNC_CAL", syncCalendar);
        contents.setValue("CAL_URI", calendarSourceUri);
        
        contents.setValue("SYNC_MAIL", syncMail) ;
        contents.setValue("MAIL_URI", mailSourceUri) ;
        contents.setValue("MAIL_ADDR", mailAddress) ;
        contents.setValue("POLL_ENABLE", enablePolling) ;
        contents.setValue("POLL_TIME", pollInterval) ;
        contents.setValue("SMS_ENABLE", enableSmsSync) ;

        contents.setValue("SYNC_ENABLE", enableAutoSync) ;
        contents.setValue("SYNC_TIME", syncInterval) ;
        
        contents.setValue("GW_APN", gatewayApn) ;
        contents.setValue("GW_IP", gatewayIp) ;
        contents.setValue("GW_PORT", gatewayPort) ;
        contents.setValue("PUSH_PORT", listeningPort) ; 

        contents.setValue("log_LEVEL", logLevel) ; 
        
        synchronized (pobj) {
            pobj.setContents(contents);
            pobj.commit();
        }

        return 0;
    }

}
