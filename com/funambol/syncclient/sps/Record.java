/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.sps;

/**
 * This class implements record definition Record contain those fields:
 * 
 * <pre>
 * 
 *  
 *   key     - record key
 *   state   - record state ('N' new, 'U' update, 'D' deleted)
 *   content - record the blackberry contactuid
 *   
 *  
 * </pre>
 * 
 */
public final class Record {

    //----------------------------------------------------------------------------
    // Private data

    private static final String FIELD_SEPARATOR = "|";

    private String key = null;

    private char state = ' ';

    private String uid = null;

    //----------------------------------------------------------------------------
    // Constructors

    public Record(String allRecord) {

        this.key = allRecord.substring(0, allRecord.indexOf(FIELD_SEPARATOR));
        this.state = allRecord.substring(allRecord.indexOf(FIELD_SEPARATOR) + 1).charAt(0);
        String tmp = allRecord.substring(allRecord.indexOf(FIELD_SEPARATOR) + 1);
        this.uid = tmp.substring(tmp.indexOf(FIELD_SEPARATOR) + 1);

    }

    public Record(String key, char state, String uid) {
        this.key = key;
        this.state = state;
        this.uid = uid;
    }
    
    public Record(String key, char state) {
        this.key = key;
        this.state = state;
    }

     
    public Record() {

    }

    /**
     * obtains key associated with this record object.
     * @param void
     * @return String: key associated with this record object
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Sets the value of key associated with this record object.
     * @param String : key value to be set 
     * @return void.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * obtains "state" associated with this record object.
     * @param void
     * @return Char: returns 'N'/'U'/'D'/'' if the record is New'N'/Udated'U'/Deleted'D'. 
     */
    public char getState() {
        return this.state;
    }

    /**
     * Sets the value of "state" associated with this record object.
     * @param char : 'N'/'U'/'D'/'' if the record is New'N'/Udated'U'/Deleted'D'. 
     * @return void.
     */
    public void setState(char state) {
        this.state = state;
    }

    /**
     * obtains the UID associated with this record object.
     * @param void
     * @return String: returns the UID of this record object
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Sets the value of "contactUID" associated with this record object.
     * @param String : it is the UID associated with the blackberry contact object. 
     * @return void.
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * Gives the string representation of this record
     * @param void
     * @return String.
     */
    public String toString() {
        return this.key + FIELD_SEPARATOR + String.valueOf(this.state) + FIELD_SEPARATOR + this.uid;
    }

}
