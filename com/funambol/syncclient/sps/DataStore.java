/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.syncclient.sps;

import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.rms.RecordStore;

import javax.microedition.pim.PIM;
import javax.microedition.pim.PIMList;

import javax.microedition.pim.PIMException;

import com.funambol.syncclient.blackberry.Configuration;
import com.funambol.syncclient.blackberry.parser.EventParser;
import com.funambol.syncclient.blackberry.parser.ParserFactory;
import com.funambol.syncclient.util.StaticDataHelper;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.util.StringMatch;


/**
 * This class provide abstract methods
 * to access device database
 *
 */
public abstract class DataStore {
      //------------------------------------------------------------- Constants

    public static final char   RECORD_STATE_NEW      = 'N'            ;
    public static final char   RECORD_STATE_DELETED  = 'D'            ;
    public static final char   RECORD_STATE_UPDATED  = 'U'            ;
    public static final char   RECORD_STATE_UNSIGNED = ' '            ;

    protected static final long PERSISTENCE_KEY = 0x50b137116d9be33cL ;

    public static final int MAX_ITEM_NUMBER = 3;
    
    //------------------------------------------------------------- Private Data

    protected PIMList pimList;
    protected int page;
    protected Enumeration items;
    protected String alertCode;

    // ------------------------------------------------------------------------

    public static DataStore getDataStore(String datastoreName) {
        
        if (Configuration.contactSourceUri.equals(datastoreName)) {
            return new ContactDataStore();
        } else if (Configuration.mailSourceUri.equals(datastoreName)) {
            return new MailDataStore();
        } else {
            return new EventDataStore();
        }
    }

    protected DataStore() {
        pimList = null;
        page = MAX_ITEM_NUMBER;
    }

    /**
     * Set last timestamp in dedicate recordStore
     * @param lastTimestamp
     * @throws DataAccessException
     **/
    public abstract void setLastTimestamp(long lastTimestamp)
    throws DataAccessException;


    /**
     * @return last timestamp from dedicate recordstore
     * @throws DataAccessException
     **/
    public abstract long getLastTimestamp()
    throws DataAccessException;

    /**
     * if record exist in database, update records
     * if record not exist in database, add record
     * @param record record to store
     * @throws DataAccessException
     **/
    public abstract Record setRecord(Record record, boolean modify)
    throws DataAccessException;

    /**
     * Delete a record from the event database.
     * @param record
     * @throws DataAccessException
     */
    public abstract void deleteRecord(Record record)
    throws DataAccessException;


    /**
     * return no deleted records from device recordstore
     *
     * @return records found
     *
     * @throws DataAccessException
     **/
    public abstract boolean getNextRecords(Vector v) throws DataAccessException;

    /**
     * return record from recordstore
     * filter by record state
     *
     * @return records found
     *
     * @throws DataAccessException
     **/
    public abstract boolean getNextRecords(Vector v, char state) throws DataAccessException;

    public abstract void startDSOperations();

    public abstract void resetModificationCursor();

    /**
     * execute commit recordstore operations
     * remove records signed as DELETED 'D'
     * mark UNSIGNED ' ' records signed as NEW 'N' and UPDATED 'U'
     *
     * @throws DataAccessException
     *
     */
    public abstract void commitDSOperations()
    throws DataAccessException;

    public abstract void appendData(String data, long key);
    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }
    public void setAlertCode(String alertCode){
        this.alertCode = alertCode;
    }
}
